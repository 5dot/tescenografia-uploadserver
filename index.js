const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();
const path = require('path');

const NodeCouchDb = require('node-couchdb');
const couch = new NodeCouchDb();
//const couchAuth = new NodeCouchDb({auth:{user:'admin',pass:'mypass'}});
const couch_databaseName = 'upfiles';

const pathRoot = __dirname;
const listen_port = 8000;


app.use(fileUpload());

app.post('/upload',function(req,res){
  if (!req.files)
    return res.status(400).send('No files were uploaded');
  let myfile = req.files.myfile;
  //console.log(myfile.name);
  //console.log(myfile.mimetype);
  //console.log(myfile.data);
  //console.log(myfile);


  var dateNow = new Date();
  var folderpath = 'uploaded-files/' + dateNow.getFullYear() + '_' + dateNow.getMonth();
  var myDocument = {};
  myDocument.name = myfile.name;
  myDocument.folderpath = folderpath;
  myDocument.mimetype = myfile.mimetype;

  couch.uniqid().then(function(ids){
    var id = ids[0];
    myDocument._id = id;

    var fullfilepath;
    fullfilepath =  myDocument.folderpath + '/' + id;
    myDocument.fullfilepath = fullfilepath;

    couch.insert(couch_databaseName, myDocument).then(
      function(data, headers, status){


        var saveFolderPath = path.join(pathRoot,folderpath);
        var fs = require('fs');
        if (!fs.existsSync(saveFolderPath)){
          fs.mkdirSync(saveFolderPath);
        }

        var saveFullPath = path.join(pathRoot,fullfilepath);


        //console.log(data);
        //console.log(headers);
        //console.log(status);
        myfile.mv(saveFullPath,function(err){
          if (err){
            return res.status(500).send(err);
          }

          res.json({id:id,url:'/download/'+id});
        });
      },
      function(err){
        res.send(err);

      });

  });


});



app.get('/download/:fileId',function(req, res){
  var fileId = req.params.fileId;

  couch.get(couch_databaseName, fileId).then(
    function (data, headers, status){
      //console.log(data.data.fullfilepath);
      var filePath = path.join(pathRoot,data.data.fullfilepath);
      var fileName = data.data.name;

      res.download(filePath,fileName);
    },
    function (err){
      res.send(err);
  });
});


app.listen(listen_port,function(){
  console.log('Listening on port ' + listen_port)
});


var createDir = function(path_to_dir){
  const fs = require('fs');
  const path = require('path');
  const targetDir = path_to_dir;
  const sep = path.sep;
  const initDir = path.isAbsolute(targetDir) ? sep : '';
  targetDir.split(sep).reduce((parentDir, childDir) => {
    const curDir = path.resolve(parentDir, childDir);
    if (!fs.existsSync(curDir)) {
      fs.mkdirSync(curDir);
    }

    return curDir;
  }, initDir);
}
